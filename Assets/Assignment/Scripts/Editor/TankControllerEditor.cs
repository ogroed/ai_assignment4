using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (TankController))]
public class TankControllerEditor : Editor
{
    public override void OnInspectorGUI ( )
    {
        var targetas = (TankController) target;

        base.OnInspectorGUI ( );

        if (GUILayout.Button ("Damage"))
        {
            targetas.TakeDamage (10f);
        }
    }
}
