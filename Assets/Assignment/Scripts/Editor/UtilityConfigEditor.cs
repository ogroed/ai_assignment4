using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (UtilityConfig))]
public class UtilityConfigEditor : Editor
{
    const string saveFolder = "Assets/Assignment/UtilityConfigs/";

    UtilityConfig currentUtilityConfig;
    Vector2 utilityNodeScrollPosition;

    private void OnEnable ( )
    {
        currentUtilityConfig = (UtilityConfig) target;
    }

    public override void OnInspectorGUI ( )
    {
        currentUtilityConfig.Setup ( );

        EditorGUI.BeginChangeCheck ( );

        using (new GUILayout.VerticalScope ( ))
        {
            using (var scrollView = new GUILayout.ScrollViewScope (utilityNodeScrollPosition))
            {
                foreach (var node in currentUtilityConfig.Utilities)
                {
                    DrawUtilityNode (node);
                }

                utilityNodeScrollPosition = scrollView.scrollPosition;
            }
        }

        if (EditorGUI.EndChangeCheck ( ))
        {
            EditorUtility.SetDirty (currentUtilityConfig);
        }
    }

    void DrawUtilityNode (UtilityNode node)
    {
        using (new GUILayout.VerticalScope (EditorStyles.helpBox))
        {
            using (new GUILayout.HorizontalScope ( ))
            {
                EditorGUILayout.LabelField (node.name);
            }

            using (new GUILayout.HorizontalScope ( ))
            {
                DrawScoreContent (node);
            }

            using (new GUILayout.HorizontalScope ( ))
            {
                float posTotal = 0f;
                float negTotal = 0f;

                foreach (var score in node.scoreConditions)
                {
                    if (score.score.x < 0f)
                    {
                        negTotal += score.score.x;
                    }
                    else if (score.score.x > 0f)
                    {
                        posTotal += score.score.x;
                    }

                    if (score.score.y < 0f)
                    {
                        negTotal += score.score.y;
                    }
                    else if (score.score.y > 0f)
                    {
                        posTotal += score.score.y;
                    }
                }

                EditorGUILayout.LabelField ($"Negative Total: {negTotal}", GUILayout.Width (200f));
                EditorGUILayout.LabelField ($"Positive Total: {posTotal}", GUILayout.Width (200f));
            }
        }
    }

    void DrawScoreContent (UtilityNode node)
    {
        GUILayout.BeginVertical (EditorStyles.helpBox);
        {
            GUILayout.BeginHorizontal ( );
            {
                EditorGUILayout.LabelField ("Score Conditions", EditorStyles.boldLabel);

                if (GUILayout.Button ("Add", EditorStyles.miniButtonLeft))
                {
                    node.scoreConditions.Add (new UtilityNode.Score ( ));
                }
                if (GUILayout.Button ("Clear", EditorStyles.miniButtonRight))
                {
                    node.scoreConditions.Clear ( );
                }
            }
            GUILayout.EndHorizontal ( );

            UtilityNode.Score toRemove = null;
            GUILayout.BeginVertical ( );
            {
                foreach (var score in node.scoreConditions)
                {
                    GUILayout.BeginVertical (EditorStyles.helpBox);
                    {
                        if (DrawScore (score))
                            toRemove = score;
                    }
                    GUILayout.EndVertical ( );
                }
            }
            GUILayout.EndVertical ( );
            if (toRemove != null)
                node.scoreConditions.Remove (toRemove);
        }
        GUILayout.EndVertical ( );
    }

    bool DrawScore (UtilityNode.Score score)
    {
        GUILayout.BeginHorizontal ( );
        {
            if (GUILayout.Button ("X", EditorStyles.miniButton))
            {
                return true;
            }
            score.condition = (TankController.ScoreCondition) DrawEnumPopup (score.condition, "Condition");

            GUILayout.BeginVertical ( );
            {
                //score.expectedValue = EditorGUILayout.Vector2Field ("Expected Value", score.expectedValue);
                //score.score = EditorGUILayout.Vector2Field ("Score", score.score);

                score.expectedValue = DrawVector2Field (score.expectedValue, "Input");
                score.score = DrawVector2Field (score.score, "Score");
            }
            GUILayout.EndVertical ( );
        }
        GUILayout.EndHorizontal ( );

        return false;
    }

    System.Enum DrawEnumPopup (System.Enum current, string label)
    {
        var changed = current;

        GUILayout.BeginHorizontal ( );
        {
            EditorGUILayout.LabelField (label, GUILayout.Width (75f));
            changed = EditorGUILayout.EnumPopup (current);
        }
        GUILayout.EndHorizontal ( );

        return changed;
    }

    Vector2 DrawVector2Field (Vector2 current, string label)
    {
        Vector2 changed = current;

        GUILayout.BeginHorizontal ( );
        {
            EditorGUILayout.LabelField (label, GUILayout.Width (75f));
            changed.x = EditorGUILayout.FloatField (changed.x);
            changed.y = EditorGUILayout.FloatField (changed.y);
        }
        GUILayout.EndHorizontal ( );

        return changed;
    }
}
