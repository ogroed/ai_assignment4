﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (MapGraph))]
public class MapGraphEditor : Editor
{
    List<GraphNodeComponent> aStarTestPath;
    List<FlowField.FlowDirection> flowFieldTestPath;

    public override void OnInspectorGUI ( )
    {
        var targetas = target as MapGraph;

        base.OnInspectorGUI ( );

        if (GUILayout.Button ("Setup Graph"))
        {
            targetas.SetupGraph ( );
            EditorUtility.SetDirty (targetas);

            foreach (var comp in targetas.GraphNodes)
            {
                EditorUtility.SetDirty (comp);
            }
        }

        GUILayout.Space (20f);

        EditorGUILayout.LabelField ("Pathfinding Test");
        if (GUILayout.Button ("Test AStar"))
        {
            var graph = targetas.GetComponent<Graph> ( );

            var start = targetas.transform.Find ("LTurnOpenSegment (2)").GetComponent<GraphNodeComponent> ( );
            var end = targetas.transform.Find ("LTurnOpenSegment (1)").GetComponent<GraphNodeComponent> ( );

            aStarTestPath = AStar.GeneratePath (graph.Nodes, graph.Edges, start, end);

            Debug.Log (aStarTestPath.Count);

            SceneView.RepaintAll ( );
        }
        if (GUILayout.Button ("Test Flow-Field"))
        {
            var graph = targetas.GetComponent<Graph> ( );

            var end = targetas.transform.Find ("LTurnOpenSegment (1)").GetComponent<GraphNodeComponent> ( );

            flowFieldTestPath = FlowField.GenerateFlowField (graph.Nodes, graph.Edges, end);

            Debug.Log (flowFieldTestPath.Count);

            SceneView.RepaintAll ( );
        }
        if (GUILayout.Button ("Clear Paths"))
        {
            aStarTestPath = null;
            flowFieldTestPath = null;
            
            SceneView.RepaintAll ( );
        }
    }

    private void OnSceneGUI ( )
    {
        if (aStarTestPath != null)
        {
            for (int i = 0; i < aStarTestPath.Count - 1; i++)
            {
                var pathDir = (aStarTestPath[i + 1].transform.position - aStarTestPath[i].transform.position);
                float pathDist = pathDir.magnitude;
                pathDir.Normalize ( );

                float angle = Vector3.SignedAngle (Vector3.forward, pathDir, Vector3.up);

                Handles.ArrowHandleCap (0, aStarTestPath[i].transform.position, Quaternion.Euler (0f, angle, 0f), pathDist - 1.5f, EventType.Repaint);

                //Handles.DrawAAPolyLine (new Vector3[ ] { aStarTestPath[i].transform.position, aStarTestPath[i + 1].transform.position });
            }
        }

        if (flowFieldTestPath != null)
        {
            for (int i = 0; i < flowFieldTestPath.Count; i++)
            {
                float angle = Vector3.SignedAngle (Vector3.forward, flowFieldTestPath[i].direction, Vector3.up);

                Handles.ArrowHandleCap (0, flowFieldTestPath[i].node.transform.position, Quaternion.Euler (0f, angle, 0f), 5f, EventType.Repaint);
            }
        }
    }
}
