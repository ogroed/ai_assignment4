using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (Graph))]
public class GraphEditor : Editor
{
    Graph targetas;

    int edgeControlHash = "GraphEdgeHandle".GetHashCode ( );
    int lastDragHandleID;

    Graph.Edge lastClickedEdge = null;
    int lastClickedEdgeIndex = -1;

    Quaternion lastCameraLookDirection;

    private void OnEnable ( )
    {
        targetas = (Graph) target;
    }

    public override void OnInspectorGUI ( )
    {
        base.OnInspectorGUI ( );

        if (lastClickedEdge != null)
        {
            GUILayout.Label ($"Edge {lastClickedEdgeIndex}");

            EditorGUI.BeginChangeCheck ( );
            lastClickedEdge.Weight = EditorGUILayout.IntSlider ("Weight", lastClickedEdge.Weight, 0, 100);
            if (EditorGUI.EndChangeCheck ( ))
            {
                EditorUtility.SetDirty (targetas);
            }
        }
    }

    protected virtual void OnSceneGUI ( )
    {
        lastCameraLookDirection = Quaternion.LookRotation (Camera.current.transform.forward, Camera.current.transform.up);

        for (int i = 0; i < targetas.Edges.Count; i++)
        {
            var edge = targetas.Edges[i];
            if (DrawEdgeHandle (edge))
            {
                //Debug.Log ($"Clicked Edge Handle on index: {i}");
                lastClickedEdgeIndex = i;
                lastClickedEdge = edge;

                GUI.changed = true;
                Repaint ( );
                break;
            }
        }
    }

    bool DrawEdgeHandle (Graph.Edge edge)
    {
        Vector3 edgeMidPoint = edge.MidPoint;

        int id = GUIUtility.GetControlID (edgeControlHash, FocusType.Passive);
        lastDragHandleID = id;

        Handles.CircleHandleCap (id, edgeMidPoint, lastCameraLookDirection, 0.5f, EventType.Repaint);
        Handles.CircleHandleCap (id, edgeMidPoint, lastCameraLookDirection, 0.5f, EventType.Layout);

        switch (Event.current.GetTypeForControl (id))
        {
            case EventType.MouseDown:
                if (HandleUtility.nearestControl == id && Event.current.button == 0)
                {
                    GUIUtility.hotControl = id;
                    Event.current.Use ( );

                    return true;
                }
                break;
        }

        return false;
    }
}
