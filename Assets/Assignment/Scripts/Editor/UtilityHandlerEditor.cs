using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

//[CustomEditor (typeof (UtilityHandler))]
public class UtilityHandlerEditor : Editor
{
    UtilityHandler targetas;

    int selectedNodeIndex;
    bool showNodes;

    private void OnEnable ( )
    {
        targetas = (UtilityHandler) target;
    }

    public override void OnInspectorGUI ( )
    {
        //base.OnInspectorGUI ( );

        EditorGUI.BeginChangeCheck ( );

        targetas.UpdateFrameSkips = EditorGUILayout.IntField ("Frame Skips", targetas.UpdateFrameSkips);

        GUILayout.BeginHorizontal ( );
        {
            if (GUILayout.Button ("Add", EditorStyles.miniButtonLeft))
            {
                targetas.UtilityNodes.Add (new UtilityNode ( ));
                showNodes = true;
            }
            if (GUILayout.Button ("Clear", EditorStyles.miniButtonRight))
            {
                targetas.UtilityNodes.Clear ( );
                showNodes = true;
            }
        }
        GUILayout.EndHorizontal ( );

        var bgStyle = new GUIStyle (EditorStyles.helpBox);

        showNodes = EditorGUILayout.Foldout (showNodes, "Utility Nodes");

        if (showNodes)
        {
            UtilityNode toRemove = null;
            GUILayout.BeginVertical ( );
            {
                for (int i = 0; i < targetas.UtilityNodes.Count; i++)
                {
                    var current = targetas.UtilityNodes[i];

                    GUILayout.BeginVertical (bgStyle);
                    {
                        GUILayout.BeginHorizontal ( );
                        {
                            if (GUILayout.Button ("X", EditorStyles.miniButton))
                            {
                                toRemove = current;
                            }
                            current.name = EditorGUILayout.TextField ("Name: ", current.name);
                        }
                        GUILayout.EndHorizontal ( );

                        GUILayout.BeginHorizontal ( );
                        {
                            current.state = (TankController.State) DrawEnumPopup (current.state, "State");
                        }
                        GUILayout.EndHorizontal ( );

                        GUILayout.Space (2.5f);

                        GUILayout.BeginHorizontal ( );
                        {
                            DrawScoreContent (current);
                        }
                        GUILayout.EndHorizontal ( );
                    }
                    GUILayout.EndVertical ( );

                    GUILayout.Space (2.5f);
                }
            }
            GUILayout.EndVertical ( );
            if (toRemove != null)
                targetas.UtilityNodes.Remove (toRemove);
        }

        if (EditorGUI.EndChangeCheck ( ))
            EditorUtility.SetDirty (targetas);
    }

    void DrawScoreContent (UtilityNode node)
    {
        GUILayout.BeginVertical (EditorStyles.helpBox);
        {
            GUILayout.BeginHorizontal ( );
            {
                EditorGUILayout.LabelField ("Score Conditions", EditorStyles.boldLabel);

                if (GUILayout.Button ("Add", EditorStyles.miniButtonLeft))
                {
                    node.scoreConditions.Add (new UtilityNode.Score ( ));
                }
                if (GUILayout.Button ("Clear", EditorStyles.miniButtonRight))
                {
                    node.scoreConditions.Clear ( );
                }
            }
            GUILayout.EndHorizontal ( );

            UtilityNode.Score toRemove = null;
            GUILayout.BeginVertical ( );
            {
                foreach (var score in node.scoreConditions)
                {
                    GUILayout.BeginVertical (EditorStyles.helpBox);
                    {
                        if (DrawScore (score))
                            toRemove = score;
                    }
                    GUILayout.EndVertical ( );
                }
            }
            GUILayout.EndVertical ( );
            if (toRemove != null)
                node.scoreConditions.Remove (toRemove);
        }
        GUILayout.EndVertical ( );
    }

    bool DrawScore (UtilityNode.Score score)
    {
        GUILayout.BeginHorizontal ( );
        {
            if (GUILayout.Button ("X", EditorStyles.miniButton))
            {
                return true;
            }
            score.condition = (TankController.ScoreCondition) DrawEnumPopup (score.condition, "Condition");
        }
        GUILayout.EndHorizontal ( );
        GUILayout.BeginHorizontal ( );
        {
            GUILayout.BeginVertical ( );
            {
                score.expectedValue = EditorGUILayout.Vector2Field ("Expected Value", score.expectedValue);
                score.score = EditorGUILayout.Vector2Field ("Score", score.score);
            }
            GUILayout.EndVertical ( );
        }
        GUILayout.EndHorizontal ( );

        return false;
    }

    System.Enum DrawEnumPopup (System.Enum current, string label)
    {
        var changed = current;

        GUILayout.BeginHorizontal ( );
        {
            EditorGUILayout.LabelField (label, GUILayout.Width (75f));
            changed = EditorGUILayout.EnumPopup (current);
        }
        GUILayout.EndHorizontal ( );

        return changed;
    }
}
