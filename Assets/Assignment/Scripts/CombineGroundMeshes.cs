﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombineGroundMeshes : MonoBehaviour
{
    void Awake ( )
    {
        var groundObjects = GameObject.FindGameObjectsWithTag ("Ground");

        CombineInstance[ ] combine = new CombineInstance[groundObjects.Length];
        Material groundMaterial = groundObjects[0].GetComponent<MeshRenderer> ( ).sharedMaterial;

        for (int i = 0; i < groundObjects.Length; i++)
        {
            var goMeshFilter = groundObjects[i].GetComponent<MeshFilter> ( );

            combine[i].mesh = goMeshFilter.mesh;

            Matrix4x4 matrix = groundObjects[i].transform.localToWorldMatrix;

            combine[i].transform = matrix;

            groundObjects[i].SetActive (false);
        }

        var meshFilter = GetComponent<MeshFilter> ( );
        meshFilter.mesh = new Mesh ( );
        meshFilter.mesh.CombineMeshes (combine);
        GetComponent<MeshCollider> ( ).sharedMesh = meshFilter.mesh;

        GetComponent<MeshRenderer> ( ).sharedMaterial = groundMaterial;

        transform.localPosition = -transform.parent.position;
    }
}
