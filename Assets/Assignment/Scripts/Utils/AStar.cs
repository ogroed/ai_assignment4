﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Priority_Queue;

public static class AStar
{
    public static List<GraphNodeComponent> GeneratePath (List<GraphNodeComponent> nodes, List<Graph.Edge> edges, Vector3 start, Vector3 end)
    {
        GraphNodeComponent startNode = nodes.OrderBy (gn => Vector3.Distance (gn.transform.position, start)).ToArray ( ) [0];
        GraphNodeComponent endNode = nodes.OrderBy (gn => Vector3.Distance (gn.transform.position, end)).ToArray ( ) [0];

        return GeneratePath (nodes, edges, startNode, endNode);
    }

    public static List<GraphNodeComponent> GeneratePath (List<GraphNodeComponent> nodes, List<Graph.Edge> edges, GraphNodeComponent start, GraphNodeComponent end)
    {
        AStarPathfinding (nodes, edges, start, end, out var path);

        // Build shortest path
        var shortestPath = new List<GraphNodeComponent> ( );

        // TODO: Path is being built the wrong way
        shortestPath.Add (end);
        var current = path[end];
        while (current != start)
        {
            shortestPath.Add (current);
            current = path[current];
        }
        shortestPath.Add (start);
        shortestPath.Reverse ( );

        return shortestPath;
    }

    // TODO: Reimplement with FastPriorityQueue, requires some refactoring in how the Graph class stores its data
    static void AStarPathfinding (List<GraphNodeComponent> nodes, List<Graph.Edge> edges, GraphNodeComponent start, GraphNodeComponent end,
        out Dictionary<GraphNodeComponent, GraphNodeComponent> path)
    {
        path = new Dictionary<GraphNodeComponent, GraphNodeComponent> ( );
        var costs = new Dictionary<GraphNodeComponent, float> ( );
        var prios = new Dictionary<GraphNodeComponent, float> ( );
        var openSet = new List<GraphNodeComponent> (nodes.Count);

        openSet.Add (start);

        costs[start] = start.cost;
        prios[start] = 0;
        path[start] = start;

        float startTime = Time.time;
        while (openSet.Count > 0)
        {
            if (Time.time - startTime > 2000f) break;
            if (openSet[0] == end) break;

            var current = openSet[0];
            openSet.RemoveAt (0);

            foreach (var neighbour in current.neighbours)
            {

                float newcost = costs[current] + (current.transform.position - neighbour.transform.position).sqrMagnitude * neighbour.cost;

                if (!costs.ContainsKey (neighbour) || newcost < costs[neighbour])
                {
                    float edgeWeight = edges.Find (e => (e.StartNode == current && e.EndNode == neighbour) || (e.EndNode == current && e.StartNode == neighbour)).Weight;
                    float prio = newcost + (current.transform.position - end.transform.position).sqrMagnitude * edgeWeight;
                    costs[neighbour] = newcost;
                    prios[neighbour] = prio;

                    if (!openSet.Contains (neighbour))
                    {
                        openSet.Add (neighbour);
                        openSet = openSet.OrderBy (gn => prios[gn]).ToList ( );
                    }

                    path[neighbour] = current;
                }
            }
        }
    }
}
