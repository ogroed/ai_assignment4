using UnityEngine;

[ExecuteInEditMode]
public class GUIUtilites : MonoBehaviour
{
    static GUIUtilites instance;

    public static GUIStyle DebugTextStyle;

    void OnEnable ( )
    {
        if (instance == null) instance = this;
        else Destroy (this);

        DebugTextStyle = new GUIStyle ( );
        DebugTextStyle.normal.textColor = Color.magenta;
    }
}
