using System.Collections;
using UnityEngine;

[System.Serializable]
public class Timer
{
    const float MaxCooldownTime = 120f;

    [SerializeField] float time;

    float lastUseTime;
    float currentCooldown;

    public float CurrentCooldown => currentCooldown;

    public void Init (MonoBehaviour owner)
    {
        owner.StartCoroutine (Run ( ));
    }

    IEnumerator Run ( )
    {
        while (true)
        {
            if (currentCooldown > 0f)
                currentCooldown = Mathf.Clamp (currentCooldown - Time.deltaTime, 0f, MaxCooldownTime);

            yield return null;
        }
    }

    public void AddTime (float value)
    {
        currentCooldown += value;
    }

    public void Use ( )
    {
        lastUseTime = Time.time;
        AddTime (time);
    }

    public bool CanUse ( )
    {
        if (currentCooldown <= 0f)
        {
            return true;
        }

        return false;
    }
}
