using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class FlowField
{
    public class FlowDirection
    {
        public GraphNodeComponent node;
        public Vector3 direction;
    }

    public static List<FlowDirection> GenerateFlowField (List<GraphNodeComponent> nodes, List<Graph.Edge> edges, GraphNodeComponent end)
    {
        var path = new List<FlowDirection> ( );
        var openSet = new List<GraphNodeComponent> ( );
        var costs = new Dictionary<GraphNodeComponent, float> ( );

        foreach (var node in nodes)
        {
            costs[node] = 65535f;
        }

        costs[end] = 0f;
        openSet.Add (end);

        float startTime = Time.time;
        while (openSet.Count > 0)
        {
            if (Time.time - startTime > 2000f) break;

            var current = openSet[0];
            openSet.RemoveAt (0);

            foreach (var neighbour in current.neighbours)
            {
                // TODO: Replace 10f by weight of edge
                float newcost = costs[current] + (current.transform.position - neighbour.transform.position).sqrMagnitude *
                    edges.Find (e => (e.StartNode == current && e.EndNode == neighbour) || (e.EndNode == current && e.StartNode == neighbour)).Weight;

                if (!costs.ContainsKey (neighbour) || newcost < costs[neighbour])
                {
                    float prio = newcost + (current.transform.position - end.transform.position).sqrMagnitude + neighbour.cost;
                    costs[neighbour] = prio;

                    if (!openSet.Contains (neighbour))
                    {
                        openSet.Add (neighbour);
                        openSet = openSet.OrderBy (gn => costs[gn]).ToList ( );
                    }

                    var flow = path.Find (fd => fd.node == neighbour);
                    if (flow == null)
                    {
                        path.Add (new FlowDirection
                        {
                            node = neighbour,
                            direction = (current.transform.position - neighbour.transform.position).normalized
                        });
                    }
                }
            }
        }

        return path;
    }
}
