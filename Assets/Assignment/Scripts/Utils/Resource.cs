using Action = System.Action;
using UnityEngine;

[System.Serializable]
public class Resource
{
    [SerializeField] float maxResource;

    float currentResource;

    public event Action onResourceChanged;
    public event Action onResourceFull;
    public event Action onResourceDepleted;

    public float CurrentResource => currentResource;
    public float CurrentResource01 => Mathf.Clamp01 (currentResource / maxResource);

    public void Init ( )
    {
        currentResource = maxResource;
    }

    public void ChangeResource (float value)
    {
        float newValue = currentResource + value;
        currentResource = Mathf.Clamp (newValue, 0f, maxResource);

        if (newValue <= 0f)
        {
            onResourceDepleted?.Invoke ( );
        }
        else if (newValue >= maxResource)
        {
            onResourceFull?.Invoke ( );
        }

        onResourceChanged?.Invoke ( );
    }
}
