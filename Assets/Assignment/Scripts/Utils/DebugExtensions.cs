using UnityEngine;

public static class DebugExtensions
{
    public static void ClearConsole ( )
    {
        var logentries = System.Type.GetType ("UnityEditor.LogEntries, UnityEditor.dll");
        var clearmethod = logentries.GetMethod ("Clear", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
        clearmethod.Invoke (null, null);
    }

    public static void LogIfSelected (this GameObject target, object message)
    {
#if UNITY_EDITOR
        if (!UnityEditor.Selection.Contains (target)) return;
#endif
        Debug.Log (message);
    }
}
