﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class Graph : MonoBehaviour
{
    [System.Serializable]
    public class Edge
    {
        public GraphNodeComponent StartNode;

        public GraphNodeComponent EndNode;

        [Range (0f, 10f)]
        public int Weight;

        public Vector3 MidPoint => (StartNode.transform.position + EndNode.transform.position) / 2f;
    }

    //List<GraphNode> graphNodes;
    //public List<GraphNode> GraphNodes => graphNodes;

    public List<GraphNodeComponent> Nodes;
    public List<Edge> Edges;

    void Awake ( )
    {
        //graphNodes = Nodes.Select (gnc => gnc.self).ToList ( );
    }

    private void Reset ( )
    {
        Nodes = GetComponentsInChildren<GraphNodeComponent> (true).ToList ( );
        Edges = new List<Edge> ( );
    }

    public void Init ( )
    {
        Reset ( );
    }

#if UNITY_EDITOR
    void OnDrawGizmos ( )
    {
        foreach (var edge in Edges)
        {
            if (edge.StartNode == null || edge.EndNode == null) continue;

            Handles.color = Color.magenta;
            Handles.Label (edge.MidPoint + Vector3.up * 0.5f, "W: " + edge.Weight.ToString ( ), GUIUtilites.DebugTextStyle);

            Handles.color = Color.red;
            Handles.DrawAAPolyLine (edge.StartNode.transform.position, edge.EndNode.transform.position);
        }
    }
#endif
}
