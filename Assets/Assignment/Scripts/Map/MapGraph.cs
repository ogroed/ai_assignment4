﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

// TODO: Fix serialization issue
[RequireComponent (typeof (Graph))]
public class MapGraph : MonoBehaviour
{
    static MapGraph instance;

    [SerializeField] float maxDistance = 15f;

    Graph graph;

    public List<GraphNodeComponent> GraphNodes => graph.Nodes;
    public List<Graph.Edge> GraphEdges => graph.Edges;

    void Awake ( )
    {
        if (instance == null) instance = this;
        else Destroy (this);

        graph = GetComponent<Graph> ( );
        //SetupGraph ( );
    }

    //public static List<GraphNode> GetPath (Vector3 start, Vector3 end)
    //{
    //    return AStar.GeneratePath (instance.graph.GraphNodes, instance.graph.Edges, start, end);
    //}

    public static float FindEdgeWeight (GraphNodeComponent start, GraphNodeComponent end)
    {
        var edge = instance.graph.Edges.Find (e => (e.StartNode == start && e.EndNode == end) || (e.EndNode == start && e.StartNode == end));

        return edge.Weight;
    }

    public static List<GraphNodeComponent> FindPath (GraphNodeComponent start, GraphNodeComponent end)
    {
        return AStar.GeneratePath (instance.graph.Nodes, instance.graph.Edges, start, end);
    }

    public void SetupGraph ( )
    {
        graph = GetComponent<Graph> ( );
        graph.Init ( );
        graph.Edges.Clear ( );

        Vector3 rayOffset = Vector3.up * 0.25f;

        // Setup up adjacent nodes in GraphNodes
        foreach (var node in graph.Nodes)
        {
            node.Init ( );
            var closest = graph.Nodes.OrderBy (gn => Vector3.Distance (gn.transform.position, node.transform.position));

            foreach (var adj in closest)
            {
                if (adj == node) continue;

                Vector3 dir = (adj.transform.position - node.transform.position);
                float dist = dir.magnitude;
                dir.Normalize ( );

                if (dist > maxDistance) continue;

                if (!Physics.Raycast (node.transform.position + rayOffset, dir, out var hit, dist, 1 << LayerMask.NameToLayer ("Solid")))
                {
                    node.neighbours.Add (adj);
                }
            }
        }

        // Setup edges in graph
        foreach (var node in graph.Nodes)
        {
            foreach (var adj in node.neighbours)
            {
                if (adj == node) continue;

                var existingEdge = graph.Edges.Find (edge => (edge.EndNode == adj && edge.StartNode == node) || (edge.EndNode == node && edge.StartNode == adj));

                if (existingEdge == null)
                {
                    var edge = new Graph.Edge ( );

                    edge.StartNode = node;
                    edge.EndNode = adj;
                    edge.Weight = 1;

                    graph.Edges.Add (edge);
                }
            }
        }
    }
}
