﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Priority_Queue;

[System.Serializable]
public class GraphNode //: FastPriorityQueueNode
{
    public int id;
    public Vector3 position;
    public List<GraphEdge> neighbours;
    [Range (0, 10)]
    public float cost;

    public GraphNode ( )
    {
        this.neighbours = new List<GraphEdge> ( );
    }

    public GraphNode (int id, Vector3 position, float cost) : this ( )
    {
        this.id = id;
        this.position = position;
    }

    public bool HasEdge (GraphNode other)
    {
        var edge = neighbours.Find (ge => (ge.start == this && ge.end == other) || (ge.end == this && ge.start == other));

        if (edge == null)
            return false;

        return true;
    }

    public void AddEdge (GraphNode other, float weight)
    {
        var newEdge = new GraphEdge (this, other, weight);
        neighbours.Add (newEdge);
    }

    public float GetEdgeCost (GraphNode other)
    {
        return neighbours.Find (ge => ge.end == other).weight;
    }
}

[System.Serializable]
public class GraphEdge
{
    public GraphNode start;
    public GraphNode end;
    public float weight;

    public GraphEdge (GraphNode start, GraphNode end, float weight)
    {
        this.start = start;
        this.end = end;
        this.weight = weight;
    }
}

public class GraphNodeComponent : MonoBehaviour
{
    //[SerializeField]
    //public GraphNode self;

    [SerializeField] public List<GraphNodeComponent> neighbours;
    [SerializeField] public float cost;

    [SerializeField]
    private string _id;

    [SerializeField] float traceFalloff = 0.25f;

    float team1Trace = 0f;
    float team2Trace = 0f;

    public float Team1Trace => team1Trace;
    public float Team2Trace => team2Trace;

    void Update ( )
    {
        if (team1Trace > 0f)
        {
            team1Trace = Mathf.Clamp01 (team1Trace - traceFalloff * Time.deltaTime);
        }
        if (team2Trace > 0f)
        {
            team2Trace = Mathf.Clamp01 (team2Trace - traceFalloff * Time.deltaTime);
        }
    }

    public void Init ( )
    {
        neighbours = new List<GraphNodeComponent> ( );
    }

    private void OnTriggerExit (Collider other)
    {
        if (other.tag == "Team1")
        {
            team1Trace = 1f;
        }
        if (other.tag == "Team2")
        {
            team2Trace = 1f;
        }
    }

    private void OnTriggerEnter (Collider other)
    {
        if (other.tag == "Team1" || other.tag == "Team2")
        {

        }
    }

    public float GetTrace (Team team)
    {
        switch (team)
        {
            case Team.Team1:
                return team2Trace;
            case Team.Team2:
                return team1Trace;
        }

        return 0f;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos ( )
    {
        Gizmos.color = Color.red;
        Gizmos.DrawCube (transform.position, Vector3.one / 2f);

        Handles.color = Color.blue;
        Handles.Label (transform.position + Vector3.up * 2f, "C: " + cost.ToString ( ), GUIUtilites.DebugTextStyle);

        if (Application.isPlaying)
        {
            Handles.Label (transform.position + Vector3.up + Vector3.right, "T1 Trace: " + team1Trace.ToString ( ), GUIUtilites.DebugTextStyle);
            Handles.Label (transform.position + Vector3.up + Vector3.left, "T2 Trace: " + team2Trace.ToString ( ), GUIUtilites.DebugTextStyle);
        }

        if (neighbours == null) return;

        foreach (var node in neighbours)
        {
            if (node == null) continue;

            //Handles.color = Color.red;
            //Handles.DrawAAPolyLine (node.transform.position, transform.position);
        }
    }
#endif
}
