﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    static CameraManager instance;

    public enum State
    {
        Overview = 0,
        Follow
    }

    [Header ("Overview Camera")]
    [SerializeField] Camera overviewCamera;
    [SerializeField] float overviewCameraTimer = 25f;

    [Header ("Follow Camera")]
    [SerializeField] Camera followCamera;
    [SerializeField] float switchTargetTimer = 15f;

    [SerializeField] State cameraState;

    State nextState;

    Transform currentTarget;
    float lastTargetChangeTime;
    float lastStateSwitch;

    Dictionary<State, StateMachine> cameraHandlers;

    void Awake ( )
    {
        if (instance == null) instance = this;
        else Destroy (this);

        cameraHandlers = new Dictionary<State, StateMachine> ( );

        cameraHandlers[State.Overview] = new StateMachine ( );
        cameraHandlers[State.Follow] = new StateMachine ( );

        cameraHandlers[State.Overview].onEnter += SetActiveCamera;
        cameraHandlers[State.Overview].onStay += OverviewCameraHandler;
        cameraHandlers[State.Overview].onExit += ( ) => { lastStateSwitch = Time.time; };

        cameraHandlers[State.Follow].onEnter += SetActiveCamera;
        cameraHandlers[State.Follow].onStay += FollowCameraHandler;
        cameraHandlers[State.Follow].onStayFixed += FollowCameraFixedHandler;
        cameraHandlers[State.Follow].onExit += ( ) => { lastStateSwitch = Time.time; };
    }

    private void Update ( )
    {
        if (nextState != cameraState)
        {
            cameraHandlers[cameraState].OnExit ( );

            cameraState = nextState;

            cameraHandlers[nextState].OnEnter ( );
        }

        cameraHandlers[cameraState].OnStay ( );
    }

    private void FixedUpdate ( )
    {
        cameraHandlers[cameraState].OnStayFixed ( );
    }

    void OverviewCameraHandler ( )
    {
        //if (Time.time - lastStateSwitch > overviewCameraTimer)
        //{
        //    nextState = State.Follow;
        //    return;
        //}

        var tankPositions = GameManager.GetTankPositions ( );

        if (tankPositions.Count > 0)
        {
            Vector3 xSize = new Vector3 (999999f, 0f);
            Vector3 ySize = new Vector3 (999999f, 0f);

            Vector3 center = Vector3.zero;

            foreach (var position in tankPositions)
            {
                center += position;
                Vector3 screenPos = position; // overviewCamera.WorldToScreenPoint (position);

                if (screenPos.x > xSize.y) xSize.y = screenPos.x;
                if (screenPos.x < xSize.x) xSize.x = screenPos.x;

                if (screenPos.z > ySize.y) ySize.y = screenPos.z;
                if (screenPos.z < ySize.x) ySize.x = screenPos.z;
            }

            center /= tankPositions.Count;

            float size = 10f;
            float sizeY = (ySize.y - ySize.x) / 2f;
            float sizeX = (xSize.y - xSize.x) / 2f;

            if (sizeX >= sizeY)
            {
                size = sizeX;
            }
            else
            {
                size = sizeY;
            }

            float cameraView = 2.0f * Mathf.Tan (0.5f * Mathf.Rad2Deg * overviewCamera.fieldOfView);
            float cameraDistance = 2.0f * size / cameraView;

            overviewCamera.transform.position = center + Vector3.up * cameraDistance;
        }
        else
        {
            overviewCamera.transform.position = Vector3.up * 80f;
        }
    }

    void FollowCameraHandler ( )
    {
        if (Time.time - lastStateSwitch > switchTargetTimer * 2f)
        {
            nextState = State.Overview;
            return;
        }

        if (Time.time - lastTargetChangeTime > switchTargetTimer)
        {
            currentTarget = GameManager.GetRandomTank (currentTarget);

            if (currentTarget == null)
            {
                nextState = State.Overview;
                return;
            }
            else
            {
                lastTargetChangeTime = Time.time;
            }
        }
    }

    void FollowCameraFixedHandler ( )
    {
        if (currentTarget != null)
            followCamera.transform.position = currentTarget.position + Vector3.up * 20f;
    }

    void SetActiveCamera ( )
    {
        switch (cameraState)
        {
            case State.Overview:
                overviewCamera.gameObject.SetActive (true);
                followCamera.gameObject.SetActive (false);
                break;
            case State.Follow:
                overviewCamera.gameObject.SetActive (false);
                followCamera.gameObject.SetActive (true);
                break;
        }
    }
}
