﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TankHUD : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI healthText;
    [SerializeField] TextMeshProUGUI stateText;
    [SerializeField] TextMeshProUGUI utilityStateText;

    [HideInInspector] public TankController targetTank;
    [HideInInspector] public UtilityHandler utilityHandler;

    void Start ( )
    {
        healthText.text = $"Health: {targetTank.Health.CurrentResource}";

        targetTank.Health.onResourceChanged += ( ) => healthText.text = $"Health: {targetTank.Health.CurrentResource.ToString ("#.#")}";
    }

    void Update ( )
    {
        if (targetTank == null)
        {
            Destroy (gameObject);
            return;
        }

        stateText.text = $"State: {targetTank.CurrentState}";
        utilityStateText.text = $"Utility State: {utilityHandler.UtilityState}";

        transform.position = targetTank.transform.position + Vector3.up;
    }
}
