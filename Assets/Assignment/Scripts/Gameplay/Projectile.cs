﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [Header ("stats")]
    [SerializeField] float damage;
    [SerializeField] float speed;
    [SerializeField] float staggerTime = 0.25f;

    [Header ("Effects")]
    [SerializeField] GameObject OnHitEffect;

    Vector3 knockbackDir;

    new Rigidbody rigidbody;
    TankController owner;

    public void Init (TankController owner)
    {
        this.owner = owner;

        rigidbody = GetComponent<Rigidbody> ( );
        rigidbody.velocity += transform.forward * speed;

        knockbackDir = transform.forward;
        knockbackDir.Scale (new Vector3 (1f, 0f, 1f));
        knockbackDir.Normalize ( );
    }

    private void OnCollisionEnter (Collision other)
    {
        //Debug.Log ("Hit Collision");

        //if (other.collider.transform.parent.gameObject.layer == LayerMask.NameToLayer ("Tank"))
        //{
        //    other.collider.GetComponentInParent<TankController> ( ).TakeDamage (damage);
        //}

        var hitEffect = GameObject.Instantiate (OnHitEffect, transform.position, Quaternion.identity);
        Destroy (hitEffect, 2f);

        Destroy (this.gameObject);
    }

    private void OnTriggerEnter (Collider other)
    {
        //Debug.Log ("Hit Trigger");

        if (other.gameObject.layer == LayerMask.NameToLayer ("Tank"))
        {
            var otherTankController = other.GetComponent<TankController> ( );

            float damageMultiplier = 1f;

            Physics.Raycast (transform.position - knockbackDir, knockbackDir, out var hit, 1f);
            float bodyHitAngle = Vector3.Angle (-knockbackDir, otherTankController.Body.forward);

            //Debug.Log (bodyHitAngle);
            Debug.DrawRay (transform.position, hit.normal * 4f, Color.magenta, 4f);

            otherTankController.Turret.rotation *= Quaternion.Euler (0f, Random.Range (-10, 10), 0f);

            // Which side of the tank is hit, front, sides or back
            if (bodyHitAngle <= 30f)
            {
                damageMultiplier = 0.8f;
            }
            else if (bodyHitAngle < 150f)
            {
                damageMultiplier = 1f;
            }
            else
            {
                damageMultiplier = 1.1f;
            }

            float hitAngle = Vector3.Angle (knockbackDir, -hit.normal);
            // Angle of attack
            if (hitAngle >= otherTankController.GlancingAngle)
            {
                float oHitAngle = hitAngle;
                hitAngle = otherTankController.GlancingAngle - (90f - hitAngle);
                float reducedDamage = Mathf.Lerp (0.25f, 1f, 1f - hitAngle / otherTankController.GlancingAngle);

                damageMultiplier *= reducedDamage;

                //Debug.Log ($"Glancing Hit at angle {hitAngle} / {oHitAngle} - dmg {reducedDamage}");
            }
            else
            {
                other.GetComponent<Rigidbody> ( ).velocity += knockbackDir * 5f;
                otherTankController.Stagger (staggerTime * damageMultiplier);
            }

            otherTankController.TakeDamage (damage * damageMultiplier);

            if (otherTankController.Health.CurrentResource01 <= 0f)
            {
                owner.NotifyKill ( );
            }

            var hitEffect = GameObject.Instantiate (OnHitEffect, transform.position, Quaternion.identity);
            Destroy (hitEffect, 2f);

            Destroy (this.gameObject);
        }
    }
}
