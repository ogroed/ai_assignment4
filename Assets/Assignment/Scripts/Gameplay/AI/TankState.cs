using UnityEngine;

public abstract class TankState
{
    protected TankController tankController;
    protected UtilityHandler utilityHandler;
    protected Transform transform;
    protected TankData tankData;

    public TankState (TankController tankController, UtilityHandler utilityHandler)
    {
        this.tankController = tankController;
        this.utilityHandler = utilityHandler;

        this.transform = tankController.transform;
        this.tankData = tankController.TankData;
    }

    public abstract void Enter ( );
    public abstract void Stay ( );
    public abstract void StayFixed ( );
    public abstract void Exit ( );
}
