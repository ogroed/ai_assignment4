using UnityEngine;

public class FindCoverTankState : TankState
{
    public FindCoverTankState (TankController tankController, UtilityHandler utilityHandler) : base (tankController, utilityHandler) { }

    public override void Enter ( )
    {
        tankController.FindPathToNode (tankController.FindOpportunityNode ( ));
        tankController.SetCurrentPathTarget (tankController.NextPathNode ( ));
    }

    public override void Exit ( ) { }

    public override void Stay ( )
    {
        if (Vector3.Distance (transform.position, tankController.CurrentPathTarget.transform.position) < 1f)
        {
            var nextNode = tankController.NextPathNode ( );

            if (nextNode != null)
            {
                tankController.SetCurrentPathTarget (nextNode);
            }
        }
    }

    public override void StayFixed ( )
    {
        tankController.MoveTowardsTarget (tankController.CurrentPathTarget.transform);
    }
}
