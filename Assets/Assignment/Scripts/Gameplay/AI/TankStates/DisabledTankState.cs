using UnityEngine;

public class DisabledTankState : TankState
{
    public DisabledTankState (TankController tankController, UtilityHandler utilityHandler) : base (tankController, utilityHandler) { }

    public override void Enter ( ) { }

    public override void Exit ( ) { }

    public override void Stay ( ) { }

    public override void StayFixed ( ) { }
}
