using UnityEngine;

public class AttackingTankState : TankState
{
    public AttackingTankState (TankController tankController, UtilityHandler utilityHandler) : base (tankController, utilityHandler) { }

    public override void Enter ( ) { }

    public override void Exit ( ) { }

    public override void Stay ( )
    {
        //if (!LookForTarget ( ) || targetTank == null)
        //{
        //    nextState = State.Idle;
        //    return;
        //}

        //if (targetTank.HasAim || targetTank.TargetAimOffset < targetAimOffset)
        //{
        //    nextState = State.Engaging;
        //}
    }

    public override void StayFixed ( )
    {
        if (tankData.targetTank == null) return;

        //if (PointTurretAtTarget (targetTank.transform))
        {
            //Debug.Log ("Tank: " + transform.name + " Fire!");

            tankController.FireProjectile ( );

            tankController.AttackCooldown.Use ( );
            
            tankController.SetScoreCondition (TankController.ScoreCondition.CanFire, 0f);
            utilityHandler.NotifyScoreChange ( );
        }
    }
}
