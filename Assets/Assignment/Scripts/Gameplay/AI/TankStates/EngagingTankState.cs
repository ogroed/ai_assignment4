using UnityEngine;

public class EngagingTankState : TankState
{
    public EngagingTankState (TankController tankController, UtilityHandler utilityHandler) : base (tankController, utilityHandler) { }

    public override void Enter ( ) { }

    public override void Exit ( ) { }

    public override void Stay ( )
    {
        //if (targetTank == null)
        //{
        //    nextState = State.Idle;
        //    return;
        //}
        //else if (!LookForTarget ( ))
        //{
        //    nextState = State.Searching;
        //    return;
        //}

        //if (attackCooldown.CanUse ( ) && hasAim) // && !targetTank.HasAim) && targetTank.TargetAimOffset > targetAimOffset)
        //{
        //    nextState = State.Attacking;
        //    return;
        //}
    }

    public override void StayFixed ( )
    {
        if (tankData.targetTank == null) return;

        tankData.hasAim = tankController.PointTurretAtTarget ( );

        float targetDistance = tankController.TargetTankDistance;

        if (targetDistance > tankController.AttackRange * 0.9f)
        {
            tankController.MoveTowardsTarget (tankData.targetTank.transform);
            //if (!tankController.MoveFindAvoid (1))
            //{
            //    float turnAngle = Vector3.SignedAngle (transform.forward, targetDirection, Vector3.up);
            //    tankController.TurnBody (Mathf.Sign (turnAngle));
            //}
        }
    }
}
