using UnityEngine;

public class RepositionTankState : TankState
{
    public RepositionTankState (TankController tankController, UtilityHandler utilityHandler) : base (tankController, utilityHandler) { }

    public override void Enter ( ) { }

    public override void Exit ( ) { }

    public override void Stay ( ) { }

    public override void StayFixed ( )
    {
        if (tankData.targetTank == null) return;

        tankData.hasAim = tankController.PointTurretAtTarget ( );

        float targetDistance = tankController.TargetTankDistance;
        Vector3 targetDirection = tankController.TargetTankDirection;

        float turnAngle = Vector3.SignedAngle (transform.forward, targetDirection, Vector3.up);

        //if (targetDistance > tankController.AttackRange * 1.1f)
        //{
        //    tankController.MoveTowardsTarget (tankData.targetTank.transform);
        //}
        //else if (targetDistance < tankController.AttackRange * 0.5f)
        //{
        //    tankController.MoveAwayFromTarget (tankData.targetTank.transform);
        //}
        //else

        // Angle body for chance of glancing blows
        //else
        {
            bool enemyRayHit = Physics.Raycast (tankData.targetTank.BarrelEnd.position + tankData.targetTank.BarrelEnd.forward,
                tankData.targetTank.BarrelEnd.forward, out var raycastHit, tankController.TargetTankDistance, 1 << LayerMask.NameToLayer ("Tank"));
            if (enemyRayHit)
            {
                if (raycastHit.transform == transform)
                {
                    float targetAim = Vector3.SignedAngle (tankData.targetTank.BarrelEnd.forward, -raycastHit.normal, Vector3.up);
                    if (Mathf.Abs (targetAim) <= tankController.GlancingAngle * 0.5f)
                    {
                        tankController.TurnBody (Mathf.Sign (targetAim));
                    }
                }
            }
        }
    }
}
