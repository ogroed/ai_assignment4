using UnityEngine;

public class OrbitTankState : TankState
{
    public OrbitTankState (TankController tankController, UtilityHandler utilityHandler) : base (tankController, utilityHandler) { }

    public override void Enter ( ) { }

    public override void Exit ( ) { }

    public override void Stay ( ) { }

    public override void StayFixed ( )
    {
        if (tankData.targetTank == null) return;

        tankData.hasAim = tankController.PointTurretAtTarget ( );

        tankController.MoveAroundPosition (tankData.targetTank.transform.position, tankController.AttackRange * 0.85f);
    }
}
