using System.Linq;
using UnityEngine;

public class FleeTankState : TankState
{
    public FleeTankState (TankController tankController, UtilityHandler utilityHandler) : base (tankController, utilityHandler) { }

    public override void Enter ( )
    {
        //if (tankData.targetTank != null)
        //{
        //    if (!tankController.FindAvoid (-1))
        //    {
        //        tankController.MoveAwayFromTarget (tankData.targetTank.transform);
        //    }
        //}

        if (tankData.targetTank != null)
        {
            var closestNode = tankController.ClosestGraphNode (transform.position);
            tankController.SetCurrentPathTarget (closestNode);
            GraphNodeComponent furthest = tankController.FindFurthestNode (tankData.targetTank.transform.position);
            tankController.SetCurrentPathTarget (furthest);
        }
    }

    public override void Exit ( ) { }

    public override void Stay ( )
    {
        if (Vector3.Distance (transform.position, tankController.CurrentPathTarget.transform.position) < 1f)
        {
            if (tankData.targetTank != null)
            {
                GraphNodeComponent furthest = tankController.FindFurthestNode (tankData.targetTank.transform.position);
                tankController.SetCurrentPathTarget (furthest);
            }
        }
    }

    public override void StayFixed ( )
    {
        tankData.hasAim = tankController.PointTurretAtTarget ( );

        tankController.MoveTowardsTarget (tankController.CurrentPathTarget.transform);
    }
}
