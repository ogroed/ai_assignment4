using UnityEngine;

public class ExploreTankState : TankState
{
    public ExploreTankState (TankController tankController, UtilityHandler utilityHandler) : base (tankController, utilityHandler)
    {

    }

    public override void Enter ( )
    {
        tankController.SetClosestGraphNode ( );
        tankController.AddSearchedNode (tankController.CurrentPathTarget);
        tankController.AddNodeToMapGraph (tankController.CurrentPathTarget);
    }

    public override void Exit ( )
    {
        tankController.SetCurrentPathTarget (null);
    }

    public override void Stay ( )
    {
        //if (LookForTarget ( ))
        //{
        //    nextState = State.Engaging;
        //    return;
        //}

        if (tankController.CurrentPathTarget != null)
        {
            if (Vector3.Distance (transform.position, tankController.CurrentPathTarget.transform.position) < 1f)
            {
                tankController.FindNextSearchNode ( );
            }
        }
    }

    public override void StayFixed ( )
    {
        if (tankController.CurrentPathTarget == null)
        {
            //Debug.Log ("No Path Target");
            return;
        }

        //Vector3 selfPosition = new Vector3 (transform.position.x, 0f, transform.position.z);
        //Vector3 targetDir = (tankController.CurrentPathTarget.transform.position - selfPosition);
        //float targetDist = targetDir.magnitude;
        //targetDir.Normalize ( );

        //float targetAngle = Vector3.SignedAngle (transform.forward, targetDir, Vector3.up);

        //float collDir = tankController.CheckCollision (1);
        //if (Mathf.Abs (targetAngle) > 2.5f && collDir == 0 && Time.time - tankData.lastCollisionTime > tankData.collisionAvoidanceTime)
        //{
        //    tankController.TurnBody (Mathf.Sign (targetAngle));
        //    float maxEase = 0.5f;
        //    tankController.Move (1, Mathf.SmoothStep (0f, maxEase, Mathf.Clamp ((1f - tankController.VelocityMagnitude01), 0f, maxEase)));
        //}
        //else
        //{
        //    if (collDir != 0)
        //    {
        //        tankData.lastCollisionTime = Time.time;
        //    }
        //    tankController.MoveAvoid (1, collDir, Mathf.Clamp (targetDist / tankController.VelocityMagnitude, 0f, 1f));
        //}

        tankController.MoveTowardsTarget (tankController.CurrentPathTarget.transform);
        tankController.PointTurrentInDirection (transform.forward);
    }
}
