using System.Collections.Generic;
using UnityEngine;

public class SearchingTankState : TankState
{
    public SearchingTankState (TankController tankController, UtilityHandler utilityHandler) : base (tankController, utilityHandler)
    {

    }

    public override void Enter ( )
    {
        // Check line of sight to previous position of target tank
        var dir = (tankData.targetLastNode.transform.position - transform.position);
        float dist = dir.magnitude;
        dir.Normalize ( );

        var rayHit = Physics.Raycast (transform.position, dir, dist, 1 << LayerMask.NameToLayer ("Solid"));

        if (rayHit)
        {
            tankController.FindPathToNode (tankData.targetLastNode);
            tankController.SetCurrentPathTarget (tankController.NextPathNode ( ));
        }
        else
        {
            tankController.SetCurrentPathTarget (tankData.targetLastNode);
        }
    }

    public override void Exit ( )
    {

    }

    public override void Stay ( )
    {
        if (tankController.CurrentPathTarget != null)
        {
            if (Vector3.Distance (transform.position, tankController.CurrentPathTarget.transform.position) < 1f)
            {
                tankController.FindNextSearchNode ( );
            }
        }
    }

    public override void StayFixed ( )
    {
        if (tankController.CurrentPathTarget == null)
        {
            //Debug.Log ("No Path Target");
            return;
        }

        tankController.MoveTowardsTarget (tankController.CurrentPathTarget.transform);
        tankController.PointTurrentInDirection (transform.forward);
    }
}
