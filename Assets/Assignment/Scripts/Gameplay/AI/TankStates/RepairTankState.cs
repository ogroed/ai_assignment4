using UnityEngine;

public class RepairTankState : TankState
{
    public RepairTankState (TankController tankController, UtilityHandler utilityHandler) : base (tankController, utilityHandler) { }

    float startTime;

    public override void Enter ( )
    {
        startTime = Time.time;

        tankController.LockState ( );
    }

    public override void Exit ( ) { }

    public override void Stay ( )
    {
        if (Time.time - startTime > tankController.RepairHealthUseTime)
        {
            tankController.RepairDamage ( );
            tankController.UnlockState ( );
        }
    }

    public override void StayFixed ( ) { }
}
