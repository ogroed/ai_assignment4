[System.Serializable]
public class TankData
{
    public bool hasAim = false;
    public float targetAimOffset;

    public float lastCollisionTime;
    public float lastCollisionAvoidDir;
    public int collisionLookAheadFrames = 30;
    public float collisionAvoidanceTime = 1f;
    public float collisionReverseDistance = 0.5f;

    // Enemy tank information
    public TankController targetTank;
    public GraphNodeComponent targetLastNode;
}
