using System;

public class StateMachine
{
    public event Action onEnter;
    public event Action onStay;
    public event Action onStayFixed;
    public event Action onExit;

    public void OnEnter ( )
    {
        onEnter?.Invoke ( );
    }

    public void OnStay ( )
    {
        onStay?.Invoke ( );
    }

    public void OnStayFixed ( )
    {
        onStayFixed?.Invoke ( );
    }

    public void OnExit ( )
    {
        onExit?.Invoke ( );
    }
}
