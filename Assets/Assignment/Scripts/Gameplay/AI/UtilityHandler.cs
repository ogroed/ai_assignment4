using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Priority_Queue;

public enum ScoreType
{
    Static,
    Range
}

[System.Serializable]
public class UtilityNode : FastPriorityQueueNode
{
    [System.Serializable]
    public class Score
    {
        public TankController.ScoreCondition condition;
        public Vector2 expectedValue;
        public Vector2 score;
    }

    public string name;
    public TankController.State state;
    public List<Score> scoreConditions;

    [HideInInspector] public float currentScore;
    public Dictionary<TankController.ScoreCondition, Score> scoreConditionsLookup;

    public UtilityNode ( )
    {
        name = GetHashCode ( ).ToString ( );
        scoreConditions = new List<Score> ( );
    }

    public void Init ( )
    {
        scoreConditionsLookup = new Dictionary<TankController.ScoreCondition, Score> ( );

        foreach (var score in scoreConditions)
        {
            scoreConditionsLookup[score.condition] = score;
        }
    }

    public void UpdateScore (TankController.ScoreCondition scoreCondition, float score)
    {
        var scoreLookup = scoreConditionsLookup[scoreCondition];

        if (score == scoreLookup.expectedValue.x)
        {
            currentScore += scoreLookup.score.x;
        }
        else if (score == scoreLookup.expectedValue.y)
        {
            currentScore += scoreLookup.score.y;
        }
    }
}

public class UtilityHandler : MonoBehaviour
{
    [SerializeField] bool logScore;
    [SerializeField] int updateFrameSkips = 10;
    //[SerializeField] List<UtilityNode> utilityNodes;
    [SerializeField] UtilityConfig utilityConfig;

    TankController tankController;

    FastPriorityQueue<UtilityNode> utility;

    //public List<UtilityNode> UtilityNodes => utilityNodes ?? (utilityNodes = new List<UtilityNode> ( ));
    public List<UtilityNode> UtilityNodes => utilityConfig.Utilities;
    public TankController.State UtilityState => utility.First.state;

    public int UpdateFrameSkips { get => updateFrameSkips; set => updateFrameSkips = value; }

    private void Awake ( )
    {
        utilityConfig = ScriptableObject.Instantiate (utilityConfig);

        tankController = GetComponent<TankController> ( );
        utility = new FastPriorityQueue<UtilityNode> (50);

        foreach (var node in UtilityNodes)
        {
            node.Init ( );
            utility.Enqueue (node, 99999f);
        }

        //tankController.onStateConditionChanged += StateConditionChanged;
    }

    private void Start ( )
    {
        ScoreConditionChanged ( );
    }

    void LateUpdate ( )
    {
        if (tankController == null) return;

        //if (Time.frameCount % updateFrameSkips == 0)
        {
            ScoreConditionChanged ( );
        }
    }

    void ScoreConditionChanged ( )
    {
#if UNITY_EDITOR
        if (UnityEditor.Selection.Contains (gameObject) && logScore)
        {
            DebugExtensions.ClearConsole ( );
        }
#endif

        foreach (var node in UtilityNodes)
        {
            float nodeScore = 0f;

            foreach (var scoreLookup in node.scoreConditions)
            {
                float conditionValue = tankController.GetScoreCondition (scoreLookup.condition);

                if (conditionValue == scoreLookup.expectedValue.x)
                {
                    nodeScore += scoreLookup.score.x;
                }
                else if (conditionValue == scoreLookup.expectedValue.y)
                {
                    nodeScore += scoreLookup.score.y;
                }
                else if (conditionValue > scoreLookup.expectedValue.x && conditionValue < scoreLookup.expectedValue.y)
                {
                    float scoreDiff = scoreLookup.score.y - scoreLookup.score.x;
                    nodeScore += Mathf.Lerp (scoreLookup.score.x, scoreLookup.score.y, conditionValue / scoreDiff);
                }
            }

            utility.UpdatePriority (node, nodeScore);

            if (logScore)
                gameObject.LogIfSelected (node.state + ": " + nodeScore);
        }

        //gameObject.LogIfSelected (utility.First.state);
    }

    public void NotifyScoreChange ( )
    {
        ScoreConditionChanged ( );
    }
}
