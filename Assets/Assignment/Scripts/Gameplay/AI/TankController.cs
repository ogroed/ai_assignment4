﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Priority_Queue;
using Stopwatch = System.Diagnostics.Stopwatch;
using UnityEngine.AI;

public class TankController : MonoBehaviour
{
    public enum State : int
    {
        None = 0, Exploring, Searching, Attacking, Retreating, Engaging, FindCover, Disabled, Reposition, Flee, Orbit, Repair
    }

    public enum ScoreCondition : int
    {
        CanFire = 0, HasCover, HasTarget, HadTarget, KilledTarget, FriendlyNearby, FriendlyHasTarget, TargetMoving,
        TargetAiming, CoverNearby, LowHealth, MediumHealth, FullHealth, TargetDistance, AimOffset, HasAim, HealthAdvantage,
        CanRepair, Disabled
    }

#region Serialized Fields
    [Header ("Debug")]
    [SerializeField] bool logBenchmark;

    [Header ("General")]
    [SerializeField] GraphNodeComponent _targetNode;
    [SerializeField] GameObject _base;
    [SerializeField] GameObject _turret;
    [SerializeField] GameObject barrelEnd;
    [SerializeField] Team team;

    [Header ("Stats")]
    [SerializeField] Resource health;
    [SerializeField] float repairHealthAmount = 25f;
    [SerializeField] float repairHealthUseTime = 1.5f;
    [SerializeField] Timer repairCooldown;
    [SerializeField] float visionRange = 50f;

    [Header ("Attacking")]
    [SerializeField] GameObject projectilePrefab;
    [SerializeField] Timer attackCooldown;
    [SerializeField] float attackDamage;
    [Range (1f, 20f)]
    [SerializeField] float attackRange;
    [SerializeField] float glancingAngle = 60f;

    [Header ("Physics")]
    [Range (10f, 50f)]
    [SerializeField] float _speed;
    [SerializeField] float maxMovementSpeed;
    [SerializeField] float bodyTurnSpeed;
    [SerializeField] float turretTurnSpeed;

    [Header ("Effects")]
    [SerializeField] ParticleSystem OnFireEffect;
    [SerializeField] ParticleSystem OnMovingEffect;
    [SerializeField] GameObject OnDeathEffect;
    [SerializeField] GameObject hudPrefab;
#endregion

#region Local Fields
    // # Other components
    new Rigidbody rigidbody;
    BoxCollider hitbox;
    UtilityHandler utilityHandler;

    // # GUI
    GameObject hudObject;

    // State machine information
    Dictionary<State, TankState> stateHandlers;
    State currentState;
    State nextState => utilityHandler.UtilityState;
    bool stateLocked = false;

    // # Tank Information
    TankData tankData;

    // # Utility Score information
    float[ ] scoreConditions;
    public event System.Action onStateConditionChanged;

    // # Physics Information
    float currentMaxMovementSpeed;

    // # Graph search information
    List<GraphNodeComponent> searched;
    List<GraphNodeComponent> mapGraph;
    GraphNodeComponent currentPathTarget;
    GraphNode currentNodeTarget;

    // # Pathfinding information
    Queue<GraphNodeComponent> currentPath;

    // # Debug
    Stopwatch debugSw;
    Queue<long> updateTicks;
#endregion

#region Public Field Accessors
    // # Map Graph node accessors
    public GraphNodeComponent TargetNode { get => _targetNode; set => _targetNode = value; }
    public GraphNodeComponent CurrentPathTarget => currentPathTarget;

    // # General information accessors
    public Team Team { get => team; set => team = value; }
    public TankData TankData => tankData;

    public Transform Turret => _turret.transform;
    public Transform Body => _base.transform;
    public Transform BarrelEnd => barrelEnd.transform;

    public Resource Health => health;
    public float RepairHealthUseTime => repairHealthUseTime;

    public bool HasAim => tankData.hasAim;
    public float TargetAimOffset => tankData.targetAimOffset;
    public float GlancingAngle => glancingAngle;
    public float AttackRange => attackRange;
    public Timer AttackCooldown => attackCooldown;

    // # State Accesssors
    public State CurrentState => currentState;
    public float GetScoreCondition (ScoreCondition target) => scoreConditions[(int) target];
    public float[ ] ScoreConditions => scoreConditions;

    // # Physics information accessors
    public float VelocityMagnitude => rigidbody.velocity.magnitude;
    public float VelocityMagnitude01 => rigidbody.velocity.magnitude / maxMovementSpeed;

    // # Target Accessors
    public float TargetTankDistance => Vector3.Distance (transform.position, tankData.targetTank.transform.position);
    public Vector3 TargetTankDirection => (tankData.targetTank.transform.position - transform.position).normalized;
#endregion

#region Setup
    void SetupStateHandlers ( )
    {
        stateHandlers = new Dictionary<State, TankState> ( );

        stateHandlers[State.Exploring] = new ExploreTankState (this, utilityHandler);
        stateHandlers[State.Searching] = new SearchingTankState (this, utilityHandler);
        stateHandlers[State.Engaging] = new EngagingTankState (this, utilityHandler);
        stateHandlers[State.Attacking] = new AttackingTankState (this, utilityHandler);
        stateHandlers[State.Reposition] = new RepositionTankState (this, utilityHandler);
        stateHandlers[State.Flee] = new FleeTankState (this, utilityHandler);
        stateHandlers[State.Orbit] = new OrbitTankState (this, utilityHandler);
        stateHandlers[State.Repair] = new RepairTankState (this, utilityHandler);
        stateHandlers[State.FindCover] = new FindCoverTankState (this, utilityHandler);
        stateHandlers[State.Disabled] = new DisabledTankState (this, utilityHandler);

        currentState = State.Exploring;
    }

    void SetupScoreConditions ( )
    {
        scoreConditions = new float[System.Enum.GetNames (typeof (ScoreCondition)).Length];

        foreach (ScoreCondition value in (ScoreCondition[ ]) System.Enum.GetValues (typeof (ScoreCondition)))
        {
            SetScoreCondition (value, 0f);
        }

        SetScoreCondition (ScoreCondition.CanFire, 1f);
        SetScoreCondition (ScoreCondition.FullHealth, 1f);
        SetScoreCondition (ScoreCondition.CanRepair, 1f);
    }
#endregion

    void Awake ( )
    {
        debugSw = new Stopwatch ( );
        updateTicks = new Queue<long> ( );

        rigidbody = GetComponent<Rigidbody> ( );
        hitbox = GetComponent<BoxCollider> ( );
        utilityHandler = GetComponent<UtilityHandler> ( );

        currentMaxMovementSpeed = maxMovementSpeed;

        tankData = new TankData ( );

        switch (team)
        {
            case Team.Team1:
                mapGraph = GameManager.team1MapGraph;
                break;
            case Team.Team2:
                mapGraph = GameManager.team2MapGraph;
                break;
        }

        searched = new List<GraphNodeComponent> ( );
        if (mapGraph == null)
            mapGraph = new List<GraphNodeComponent> ( );

        SetupStateHandlers ( );

        health.Init ( );
        health.onResourceDepleted += Die;
        health.onResourceChanged += SetHealthScoreCondition;

        attackCooldown.Init (this);
        repairCooldown.Init (this);

        SetupScoreConditions ( );
    }

    void Start ( )
    {
        hudObject = GameObject.Instantiate (hudPrefab, GameManager.WorldCanvas.transform);
        hudObject.GetComponent<TankHUD> ( ).targetTank = this;
        hudObject.GetComponent<TankHUD> ( ).utilityHandler = utilityHandler;

        stateHandlers[currentState].Enter ( );

        tag = team.ToString ( );
        SetTagRecursive (transform);

    }

    void Update ( )
    {
#if UNITY_EDITOR
        debugSw.Restart ( );
#endif

        EvaluateWorld ( );
        ChangeToNextState ( );

        stateHandlers[currentState].Stay ( );

#if UNITY_EDITOR
        debugSw.Stop ( );
        updateTicks.Enqueue (debugSw.ElapsedTicks);
        if (updateTicks.Count > 100) updateTicks.Dequeue ( );

        if (UnityEditor.Selection.Contains (gameObject) && logBenchmark)
        {
            DebugExtensions.ClearConsole ( );
            Debug.Log ($"Tank Update Time:\n\t Avg: {updateTicks.Average()} ticks \n\t High: {updateTicks.Max()} \n\t Low: {updateTicks.Min()}");
        }
#endif

        if (VelocityMagnitude > 1f)
        {
            if (!OnMovingEffect.isPlaying)
                OnMovingEffect.Play ( );
        }
        else
        {
            if (OnMovingEffect.isPlaying)
                OnMovingEffect.Stop ( );
        }
    }

    void FixedUpdate ( )
    {
        stateHandlers[currentState].StayFixed ( );

        rigidbody.velocity = Vector3.ClampMagnitude (rigidbody.velocity, currentMaxMovementSpeed);
    }

    void ChangeToNextState ( )
    {
        if (stateLocked)
        {
            return;
        }

        if (nextState != currentState)
        {
            stateHandlers[currentState].Exit ( );
            stateHandlers[nextState].Enter ( );

            currentState = nextState;
        }
    }

    public void SetNextState (State state)
    {
        //nextState = state;
    }

    public void LockState ( ) => stateLocked = true;
    public void UnlockState ( ) => stateLocked = false;

#region Utility Score stuff
    void EvaluateWorld ( )
    {
        if (repairCooldown.CanUse ( ))
        {
            SetScoreCondition (ScoreCondition.CanRepair, 1f);
        }
        else
        {
            SetScoreCondition (ScoreCondition.CanRepair, 0f);
        }

        if (LookForTarget ( ))
        {
            SetScoreCondition (ScoreCondition.AimOffset, Mathf.Abs (tankData.targetAimOffset));

            //if (tankData.hasAim)
            if (Mathf.Abs (tankData.targetAimOffset) < 2.5f)
            {
                SetScoreCondition (ScoreCondition.HasAim, 1f);
            }
            else
            {
                SetScoreCondition (ScoreCondition.HasAim, 0f);
            }

            if (attackCooldown.CanUse ( ) && GetScoreCondition (ScoreCondition.CanFire) == 0f)
            {
                SetScoreCondition (ScoreCondition.CanFire, 1f);
            }

            if (health.CurrentResource >= tankData.targetTank.health.CurrentResource)
            {
                SetScoreCondition (ScoreCondition.HealthAdvantage, 1f);
            }
            else
            {
                SetScoreCondition (ScoreCondition.HealthAdvantage, 0f);
            }
        }
        else
        {
            SetScoreCondition (ScoreCondition.AimOffset, 180f);
        }
    }

    public void SetScoreCondition (ScoreCondition scoreCondition, float score)
    {
        scoreConditions[(int) scoreCondition] = score;

        onStateConditionChanged?.Invoke ( );
    }

    void SetHealthScoreCondition ( )
    {
        float currentResourcePercent = health.CurrentResource01;

        if (currentResourcePercent > 0.75f)
        {
            SetScoreCondition (ScoreCondition.FullHealth, 1f);
            SetScoreCondition (ScoreCondition.MediumHealth, 0f);
            SetScoreCondition (ScoreCondition.LowHealth, 0f);
        }
        else if (currentResourcePercent > 0.5f)
        {
            SetScoreCondition (ScoreCondition.MediumHealth, 1f);
            SetScoreCondition (ScoreCondition.LowHealth, 0f);
            SetScoreCondition (ScoreCondition.FullHealth, 0f);
        }
        else
        {
            SetScoreCondition (ScoreCondition.LowHealth, 1f);
            SetScoreCondition (ScoreCondition.MediumHealth, 0f);
            SetScoreCondition (ScoreCondition.FullHealth, 0f);
        }
    }

    public void NotifyKill ( )
    {
        SetScoreCondition (ScoreCondition.HadTarget, 0f);
    }
#endregion

#region General
    void SetTagRecursive (Transform parent)
    {
        foreach (Transform child in parent)
        {
            child.tag = tag;
            SetTagRecursive (child);
        }
    }

    public void SetTeam (Team team)
    {
        this.team = team;

        Color teamColor = Color.gray;
        switch (team)
        {
            case Team.Team1:
                teamColor = Color.blue;
                break;
            case Team.Team2:
                teamColor = Color.red;
                break;
        }

        _base.GetComponent<MeshRenderer> ( ).material.color = teamColor;
        _turret.GetComponent<MeshRenderer> ( ).material.color = teamColor;
    }

    void Die ( )
    {
        GameObject.Destroy (hudObject);

        switch (team)
        {
            case Team.Team1:
                if (GameManager.team1MapGraph == null)
                {
                    GameManager.team1MapGraph = mapGraph;
                }
                else
                {
                    GameManager.team1MapGraph = GameManager.team1MapGraph.Union (mapGraph).ToList ( );
                }
                break;
            case Team.Team2:
                if (GameManager.team2MapGraph == null)
                {
                    GameManager.team2MapGraph = mapGraph;
                }
                else
                {
                    GameManager.team2MapGraph = GameManager.team2MapGraph.Union (mapGraph).ToList ( );
                }
                break;
        }

        var deathEffect = GameObject.Instantiate (OnDeathEffect, transform.position, transform.rotation);
        Destroy (deathEffect, 2f);

        gameObject.SetActive (false);
        GameManager.RespawnTank (this);
    }

    public void TakeDamage (float amount)
    {
        if (amount > 0f) amount = -amount;

        health.ChangeResource (amount);

        currentMaxMovementSpeed = Mathf.Clamp (health.CurrentResource01, 0.25f, 1f) * maxMovementSpeed;
    }

    public void RepairDamage ( )
    {
        if (repairCooldown.CanUse ( ))
        {
            health.ChangeResource (repairHealthAmount);
            repairCooldown.Use ( );
        }
    }

    public void Stagger (float time)
    {
        StartCoroutine (StaggerRoutine (time));
    }

    IEnumerator StaggerRoutine (float time)
    {
        SetScoreCondition (ScoreCondition.Disabled, 1f);
        attackCooldown.AddTime (time);

        yield return new WaitForSeconds (time);

        SetScoreCondition (ScoreCondition.Disabled, 0f);
    }
#endregion

#region Target Tank stuff
    // TODO: Optimize as it's pretty expensive to run
    /// <summary>
    /// Finds closest enemy tank in range
    /// </summary>
    /// <returns></returns>
    public TankController HasTarget ( )
    {
        // First do general physics cast to find nearby targets
        var circleHits = Physics.OverlapSphere (transform.position, visionRange, 1 << LayerMask.NameToLayer ("Tank"));

        if (circleHits.Length > 0)
        {
            // Select out enemy tanks and order them by distance
            circleHits = circleHits.Where (coll => coll.tag != tag && coll.transform != transform).OrderBy (coll => Vector3.Distance (transform.position, coll.transform.position)).ToArray ( );

            // Establish layermask that blocks line of sight
            int blockingMask = 1 << LayerMask.NameToLayer ("Solid") | 1 << LayerMask.NameToLayer ("Tank");

            // Raycast to each target
            foreach (var chit in circleHits)
            {
                Vector3 targetDir = (chit.transform.position - barrelEnd.transform.position);
                float targetDist = targetDir.magnitude;
                targetDir.Normalize ( );

                bool rayHit = Physics.Raycast (barrelEnd.transform.position - barrelEnd.transform.forward, targetDir, out var rayHitInfo, targetDist, blockingMask);
                if (rayHit) rayHit = rayHitInfo.transform.tag.Contains ("Team") && rayHitInfo.transform.tag != transform.tag ? false : true;

                if (!rayHit)
                {
                    Debug.DrawRay (barrelEnd.transform.position, targetDir * targetDist, Color.green, 0f);

                    SetScoreCondition (ScoreCondition.TargetDistance, rayHitInfo.distance);

                    return chit.GetComponent<TankController> ( );
                }
                else
                {
                    SetScoreCondition (ScoreCondition.TargetDistance, 999f);

                    Debug.DrawRay (barrelEnd.transform.position, targetDir * targetDist, Color.red, 0f);
                }
            }
        }

        return null;
    }

    /// <summary>
    /// Looks for nearby target and updates score conditions
    /// </summary>
    /// <returns></returns>
    public bool LookForTarget ( )
    {
        tankData.targetTank = HasTarget ( );

        if (tankData.targetTank != null)
        {
            tankData.targetLastNode = ClosestGraphNode (tankData.targetTank.transform.position);

            tankData.targetAimOffset = Vector3.SignedAngle (barrelEnd.transform.forward, TargetTankDirection, Vector3.up);

            SetScoreCondition (ScoreCondition.HasTarget, 1f);
            SetScoreCondition (ScoreCondition.HadTarget, 1f);
            return true;
        }
        else
        {
            tankData.targetAimOffset = 180f;

            SetScoreCondition (ScoreCondition.HasTarget, 0f);
        }

        return false;
    }
#endregion

#region Map/Graph stuff
    public GraphNodeComponent ClosestGraphNode (Vector3 position)
    {
        var nodesOrdered = GameManager.MapGraph.GraphNodes.OrderBy (gnc => Vector3.Distance (position, gnc.transform.position)).ToArray ( );

        return nodesOrdered[0];
    }

    public void SetClosestGraphNode ( )
    {
        _targetNode = ClosestGraphNode (transform.position);

        currentPathTarget = _targetNode;
    }

    public void SetCurrentPathTarget (GraphNodeComponent node) => currentPathTarget = node;
    public void AddSearchedNode (GraphNodeComponent node) => searched.Add (currentPathTarget);
    public void ClearSearchedNodes ( ) => searched.Clear ( );
    public void AddNodeToMapGraph (GraphNodeComponent node)
    {
        if (!mapGraph.Contains (node))
        {
            mapGraph.Add (node);
        }

        foreach (var neighbour in node.neighbours)
        {
            if (!mapGraph.Contains (neighbour))
            {
                mapGraph.Add (neighbour);
            }
        }
    }

    public void SearchMapSimple ( )
    {
        var newTarget = currentPathTarget.neighbours.Find (gn => !searched.Contains (gn));

        if (newTarget != null)
        {
            Debug.Log (currentPathTarget.name);
            searched.Add (currentPathTarget);
        }
        else
        {
            searched = new List<GraphNodeComponent> ( );
            newTarget = currentPathTarget.neighbours.Find (gn => !searched.Contains (gn));
        }

        currentPathTarget = newTarget;
    }

    public void FindNextSearchNode ( )
    {
        if (currentPath != null && currentPath.Count > 0)
        {
            currentPathTarget = currentPath.Dequeue ( );
            return;
        }

        // Check if current node target has unexplored nodes
        var enemyTraceSearch = currentPathTarget.neighbours.OrderByDescending (gnc => gnc.GetTrace (team)).Where (gnc => gnc.GetTrace (team) != 0f).ToArray ( );

        var newTarget = currentPathTarget.neighbours.Find (gn => !searched.Contains (gn));
        if (enemyTraceSearch.Length > 0f)
            newTarget = enemyTraceSearch[0];

        // Find uneplored nodes in the local graph
        if (newTarget == null)
        {
            var unexplored = mapGraph.Where (gn => !searched.Contains (gn)).OrderBy (gn => Vector3.Distance (transform.position, gn.transform.position)).ToArray ( );

            if (unexplored.Length == 0)
            {
                newTarget = currentPathTarget;
                searched.Clear ( );
            }
            else
            {
                // Do pathfinding
                currentPath = new Queue<GraphNodeComponent> (MapGraph.FindPath (currentPathTarget, unexplored[0]));
                newTarget = currentPath.Dequeue ( );
            }
        }

        // Add information about the next node to the local graph
        if (newTarget != null)
        {
            searched.Add (newTarget);

            AddNodeToMapGraph (newTarget);
        }

        currentPathTarget = newTarget;
    }

    public void FindPathToNode (GraphNodeComponent node)
    {
        currentPath = new Queue<GraphNodeComponent> (AStar.GeneratePath (mapGraph, GameManager.MapGraph.GraphEdges, ClosestGraphNode (transform.position), node));
    }

    public GraphNodeComponent FindFurthestNode (Vector3 target)
    {
        Vector3 targetDir = Vector3.zero;
        float targetDist = 0f;

        var sortedNeighbours = currentPathTarget.neighbours
            .OrderByDescending (gnc => Vector3.Distance (gnc.transform.position, target))
            .ThenBy (gnc =>
            {
                targetDir = (target - gnc.transform.position);
                targetDist = targetDir.magnitude;
                targetDir.Normalize ( );

                return Physics.Raycast (gnc.transform.position, targetDir, targetDist, 1 << LayerMask.NameToLayer ("Solid")) ? 1f : 0f;
            })
            .ToArray ( );

        return sortedNeighbours[0];
    }

    public GraphNodeComponent FindOpportunityNode ( )
    {
        var sortedGraph = mapGraph
            .OrderBy (gnc => gnc.neighbours.Count)
            //.OrderByDescending (gnc => Vector3.Distance (transform.position, gnc.transform.position))
            .ThenBy (gnc => Vector3.Distance (transform.position, gnc.transform.position))
            .ToArray ( );

        return sortedGraph[0];
    }

    public GraphNodeComponent NextPathNode ( )
    {
        if (currentPath.Count > 0)
        {
            return currentPath.Dequeue ( );
        }

        return null;
    }

    void PrintVisibleMapInfo ( )
    {
        DebugExtensions.ClearConsole ( );
        Debug.Log (mapGraph.Count);
        foreach (var node in mapGraph)
        {
            Debug.Log (node.name);
        }
    }
#endregion

#region Actuators
    public void FireProjectile ( )
    {
        OnFireEffect.Stop ( );
        OnFireEffect.Play ( );

        var newProjectile = GameObject.Instantiate (projectilePrefab, barrelEnd.transform.position, barrelEnd.transform.rotation);
        newProjectile.transform.forward = TargetTankDirection;
        newProjectile.GetComponent<Projectile> ( ).Init (this);

        Destroy (newProjectile, 10f);
    }

    public void AddForce (Vector3 force)
    {
        rigidbody.velocity += force;
    }

    public bool FindAvoid (int moveDir)
    {
        //float avoidDir = CheckCollision (moveDir);
        float avoidDir = CheckNavMesh (moveDir);
        tankData.lastCollisionAvoidDir = avoidDir;

        if (avoidDir != 0f)
        {
            gameObject.LogIfSelected (tankData.lastCollisionAvoidDir);

            tankData.lastCollisionTime = Time.time;

            return true;
        }

        tankData.lastCollisionTime = 0f;

        return false;
    }

    public bool MoveFindAvoid (int moveDir, float ease = 1f)
    {
        bool coll = FindAvoid (moveDir);

        MoveAvoid (moveDir, ease);

        return coll;
    }

    public void MoveAvoid (int moveDir, float ease = 1f)
    {
        if (Time.time - tankData.lastCollisionTime < tankData.collisionAvoidanceTime)
        {
            if (Mathf.Abs (tankData.lastCollisionAvoidDir) < tankData.collisionReverseDistance)
            {
                TurnBody (Mathf.Sign (tankData.lastCollisionAvoidDir));
                Move (-moveDir, ease * 0.75f);
            }
            else
            {
                TurnBody (Mathf.Sign (tankData.lastCollisionAvoidDir));
                Move (moveDir, ease * 0.75f);
            }
        }
        else
        {
            Move (moveDir, ease);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dir">1 for forward, -1 for backward</param>
    /// <param name="ease">Value between 0 and 1, applied to acceleration</param>
    public void Move (int dir, float ease = 1f)
    {
        Vector3 movementDelta = dir * transform.forward * _speed * ease * Time.fixedDeltaTime;

        if (dir == -1)
        {
            movementDelta *= 0.75f;
        }

        if (float.IsNaN (movementDelta.x))
        {
            movementDelta = Vector3.zero;
        }

        rigidbody.velocity += movementDelta;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dir">-1 for left, 1 for right</param>
    public void TurnBody (float dir)
    {
        transform.rotation *= Quaternion.Euler (0f, bodyTurnSpeed * Mathf.Clamp ((1f - VelocityMagnitude01), 0.3f, 1f) * dir * Time.fixedDeltaTime, 0f);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dir">-1 for left, 1 for right</param>
    public void TurnTurret (float dir)
    {
        _turret.transform.rotation *= Quaternion.Euler (0f, turretTurnSpeed * dir * Time.fixedDeltaTime, 0f);
    }

    public bool PointTurretAtTarget ( )
    {
        if (Mathf.Abs (tankData.targetAimOffset) > 1.5f)
        {
            TurnTurret (Mathf.Sign (tankData.targetAimOffset));
            return false;
        }
        else
        {
            return true;
        }
    }

    public void PointTurrentInDirection (Vector3 direction)
    {
        float sangle = Vector3.SignedAngle (barrelEnd.transform.forward, direction, Vector3.up);

        if (Mathf.Abs (sangle) > 0.5f)
        {
            TurnTurret (Mathf.Sign (sangle));
        }
    }

    public void MoveAroundPosition (Vector3 position, float radius, float turnDir = 1f)
    {
        //if (FindAvoid (1))
        //{
        //    MoveAvoid (1);
        //    return;
        //}

        Vector3 targetDir = (position - transform.position);
        float targetDist = targetDir.magnitude;
        targetDir.Normalize ( );

        float sangle = Vector3.SignedAngle (transform.forward, targetDir, Vector3.up);
        float absangle = Mathf.Abs (sangle);
        //float turnDir = Mathf.Sign (sangle);
        float ease = Mathf.Abs (radius - targetDist) / radius;
        float ease2 = Mathf.Clamp (ease, 0.3f, 1f);

        if (targetDist > radius)
        {
            if (absangle > 80f)
            {
                TurnBody (turnDir);
            }
            else
            {
                MoveFindAvoid (1);
            }
        }
        else if (targetDist < radius)
        {
            if (ease < 0.05f && absangle > 80f)
            {
                TurnBody (turnDir);
                MoveFindAvoid (1, ease2);
            }
            else
            {
                MoveFindAvoid (1, ease2);
            }
        }
    }

    public void MoveTowardsTarget (Transform target)
    {
        Vector3 selfPosition = new Vector3 (transform.position.x, 0f, transform.position.z);
        Vector3 targetDir = (target.position - selfPosition);
        float targetDist = targetDir.magnitude;
        targetDir.Normalize ( );

        if (FindAvoid (1))
        {
            MoveAvoid (1);
            return;
        }

        float targetAngle = Vector3.SignedAngle (transform.forward, targetDir, Vector3.up);

        if (Mathf.Abs (targetAngle) > 2.5f)
        {
            TurnBody (Mathf.Sign (targetAngle));
        }
        else
        {
            MoveAvoid (1);
        }
    }

    public void MoveAwayFromTarget (Transform target)
    {
        Vector3 selfPosition = new Vector3 (transform.position.x, 0f, transform.position.z);
        Vector3 targetDir = (target.position - selfPosition);
        float targetDist = targetDir.magnitude;
        targetDir.Normalize ( );

        if (FindAvoid (-1))
        {
            MoveAvoid (-1);
            return;
        }

        float targetAngle = Vector3.SignedAngle (transform.forward, targetDir, Vector3.up);

        if (Mathf.Abs (targetAngle) > 2.5f)
        {
            TurnBody (Mathf.Sign (targetAngle));
        }
        else
        {
            MoveAvoid (-1);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="direction">1 for forward, -1 for backward</param>
    /// <returns>0 for no collision, 1 for right, -1 for left</returns>
    public float CheckCollision (int direction = 1)
    {
        // Establish ray origins for checking collisions
        Vector3 frontCenter = transform.position + hitbox.center + (transform.forward * (direction * (hitbox.size.z / 2f) + 0.001f));
        float tankWidth = hitbox.size.x / 2f; // - 0.05f;
        Vector3 frontLeft = frontCenter - transform.right * tankWidth;
        Vector3 frontRight = frontCenter + transform.right * tankWidth;

        int solidLayerMask = 1 << LayerMask.NameToLayer ("Solid") | 1 << LayerMask.NameToLayer ("Tank");
        float checkDistance = Mathf.Clamp (VelocityMagnitude * Time.fixedDeltaTime * tankData.collisionLookAheadFrames, 0.5f, 15f);

        Debug.DrawRay (frontLeft, transform.forward * checkDistance * direction, Color.cyan, 0f);
        Debug.DrawRay (frontRight, transform.forward * checkDistance * direction, Color.cyan, 0f);

        //var leftHit = Physics.Raycast (frontLeft, transform.forward * direction, out var left, checkDistance, solidLayerMask);
        //var rightHit = Physics.Raycast (frontRight, transform.forward * direction, out var right, checkDistance, solidLayerMask);

        float collSize = 0.5f;
        var raycastHit = new RaycastHit ( );

        var leftHit = Physics.BoxCast (frontLeft + transform.forward * (collSize / 2f), new Vector3 (collSize, collSize, collSize) / 2f,
            transform.forward, out raycastHit, transform.rotation, checkDistance, solidLayerMask);

        if (leftHit)
        {
            return raycastHit.distance;
        }

        var rightHit = Physics.BoxCast (frontRight + transform.forward * (collSize / 2f), new Vector3 (collSize, collSize, collSize) / 2f,
            transform.forward, out raycastHit, transform.rotation, checkDistance, solidLayerMask);

        if (rightHit)
        {
            return -raycastHit.distance;
        }

        //if (leftHit && rightHit)
        //{
        //    if (left.distance == right.distance)
        //    {
        //        return left.distance;
        //    }
        //    else if (left.distance > right.distance)
        //    {
        //        return right.distance;
        //    }
        //    else
        //    {
        //        return -left.distance;
        //    }
        //}
        //else if (leftHit)
        //{
        //    return -left.distance;
        //}
        //else if (rightHit)
        //{
        //    return right.distance;
        //}

        //return CheckNavMesh (direction);
        return 0f;
    }

    public float CheckNavMesh (int direction = 1)
    {
        float bodySize = (hitbox.size.z / 2f);
        NavMeshPath path = new NavMeshPath ( );

        float checkDistance = Mathf.Clamp (VelocityMagnitude * Time.fixedDeltaTime * tankData.collisionLookAheadFrames, 1f, 15f);
        Vector3 groundPos = Vector3.Scale (transform.position, new Vector3 (1f, 0f, 1f));

        float tankWidth = hitbox.size.x / 2f - 0.05f;
        Vector3 frontLeft = transform.right * tankWidth;
        Vector3 frontRight = transform.right * tankWidth;

        if (NavMesh.CalculatePath (groundPos, groundPos + Quaternion.Euler (0f, -30f, 0f) * Vector3.forward * checkDistance / 2f, 0, path))
        {
            return 1f;
        }

        if (NavMesh.CalculatePath (groundPos, groundPos + Quaternion.Euler (0f, 30f, 0f) * Vector3.forward * checkDistance / 2f, 0, path))
        {
            return -1f;
        }

        if (NavMesh.CalculatePath (groundPos, groundPos + Vector3.forward * checkDistance, 0, path))
        {
            return 0f;
        }

        return CheckCollision (direction);
        //return 0f;
    }
#endregion

#region Debug
#if UNITY_EDITOR
    void OnDrawGizmosSelected ( )
    {
        if (searched == null) return;

        Gizmos.color = new Color (0f, 0f, 1f, 0.5f);
        foreach (var node in searched)
        {
            Gizmos.DrawCube (node.transform.position, Vector3.one * 1f);
        }

        Gizmos.color = Color.green;
        foreach (var node in mapGraph)
        {
            Gizmos.DrawCube (node.transform.position, Vector3.one * 0.75f);
        }
    }

    void OnDrawGizmos ( )
    {
        //if (health != null)
        //{
        //    var style = new GUIStyle ( );
        //    style.normal.textColor = Color.magenta;

        //    UnityEditor.Handles.Label (transform.position + Vector3.up, $"Health: {health.CurrentResource}", style);
        //    UnityEditor.Handles.Label (transform.position + Vector3.up * 1.15f, $"State: {currentState}", style);
        //}

        UnityEditor.Handles.ArrowHandleCap (0, transform.position, transform.rotation, 2f, EventType.Repaint);
    }
#endif
#endregion
}
