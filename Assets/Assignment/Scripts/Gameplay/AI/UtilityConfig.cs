using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "UtilityConfig", menuName = "UtilityConfig", order = 0)]
public class UtilityConfig : ScriptableObject
{
    [SerializeField, HideInInspector] List<UtilityNode> utilities;

    public List<UtilityNode> Utilities { get => utilities; set => utilities = value; }

    public void Setup ( )
    {
        var states = (TankController.State[ ]) System.Enum.GetValues (typeof (TankController.State));

        if (utilities == null)
        {
            utilities = new List<UtilityNode> (states.Length);

            for (int i = 0; i < states.Length; i++)
            {
                if (states[i] == TankController.State.None) continue;

                utilities[i] = new UtilityNode ( );

                utilities[i].state = states[i];
                utilities[i].name = states[i].ToString ( );
            }
        }
        else if (utilities.Count < states.Length)
        {
            foreach (var state in states)
            {
                if (state == TankController.State.None) continue;

                if (utilities.Find (un => un.state == state) == null)
                {
                    var newNode = new UtilityNode ( );

                    newNode.state = state;
                    newNode.name = state.ToString ( );

                    utilities.Add (newNode);
                }
            }
        }
        else if (utilities.Count >= states.Length)
        {
            utilities.RemoveAll (un => un.state == TankController.State.None || (int) un.state >= states.Length);
        }
    }
}
