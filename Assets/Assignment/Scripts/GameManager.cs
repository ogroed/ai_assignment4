using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum Team
{
    None = 0, Team1, Team2
}

public class GameManager : MonoBehaviour
{
    static GameManager instance;

    static MapGraph mapGraph;
    public static MapGraph MapGraph => mapGraph;

    [SerializeField] GameObject tankPrefab;
    [SerializeField] List<GraphNodeComponent> team1SpawnPoints;
    [SerializeField] List<GraphNodeComponent> team2SpawnPoints;
    [SerializeField] int tanksPerTeam;

    [SerializeField] float respawnTime = 5f;
    [SerializeField] float roundTime = 120f;
    [SerializeField] int rounds = 5;
    [SerializeField] float nextRoundCooldown = 5f;

    List<GameObject> team1Tanks;
    List<GameObject> team2Tanks;

    public static List<GraphNodeComponent> team1MapGraph;
    public static List<GraphNodeComponent> team2MapGraph;

    public static GameObject WorldCanvas;

    int team1RoundScore;
    int team2RoundScore;

    int team1MatchScore;
    int team2MatchScore;

    int currentRound;
    float currentRoundTime;

    bool matchOver = false;

    Texture2D boxBackgroundTexture;

    void Awake ( )
    {
        if (instance == null) instance = this;
        else Destroy (this);

        mapGraph = FindObjectOfType<MapGraph> ( );

        WorldCanvas = GameObject.Find ("WorldCanvas");

        boxBackgroundTexture = CreateTexture (300, 300, new Color (0f, 0f, 0f, 0.25f));
    }

    void Start ( )
    {
        team1Tanks = new List<GameObject> ( );
        team2Tanks = new List<GameObject> ( );

        NewRound (true);
    }

    public static void NotifyKill (Team team)
    {
        switch (team)
        {
            case Team.Team1:
                instance.team2RoundScore++;
                break;
            case Team.Team2:
                instance.team1RoundScore++;
                break;
        }
    }

    IEnumerator WaitForGameRestart ( )
    {
        yield return new WaitForSeconds (1f);

        RestartGame ( );
    }

    void RestartGame ( )
    {
        team1MatchScore = 0;
        team2MatchScore = 0;

        team1RoundScore = 0;
        team2RoundScore = 0;

        currentRound = 0;

        NewRound (true);
    }

    IEnumerator RoundTimer ( )
    {
        while (currentRoundTime > 0f)
        {
            currentRoundTime -= Time.deltaTime;
            yield return null;
        }

        NewRound ( );
    }

    IEnumerator WaitForNextRound ( )
    {
        yield return new WaitForSeconds (nextRoundCooldown);

        SpawnTeams ( );
        currentRoundTime = roundTime;

        StartCoroutine (RoundTimer ( ));
    }

    void NewRound (bool first = false)
    {
        StopAllCoroutines ( );
        DespawnTeams ( );

        currentRound++;

        if (currentRound <= rounds)
        {
            StartCoroutine (WaitForNextRound ( ));
        }
        else
        {
            matchOver = true;
            return;
        }

        if (first) return;

        if (team1RoundScore > team2RoundScore)
        {
            team1MatchScore++;
        }
        else if (team2RoundScore > team1RoundScore)
        {
            team2MatchScore++;
        }

        team1RoundScore = 0;
        team2RoundScore = 0;
    }

    void DespawnTeams ( )
    {
        for (int i = 0; i < team1Tanks.Count; i++)
        {
            Destroy (team1Tanks[i]);
        }

        for (int i = 0; i < team2Tanks.Count; i++)
        {
            Destroy (team2Tanks[i]);
        }
    }

    void SpawnTeams ( )
    {
        for (int i = 0; i < tanksPerTeam; i++)
        {
            SpawnTank (Team.Team1);
            SpawnTank (Team.Team2);
        }
    }

    void SpawnTank (Team team)
    {
        var newTank = GameObject.Instantiate (tankPrefab, Vector3.zero, Quaternion.identity);
        var tankComponent = newTank.GetComponent<TankController> ( );
        tankComponent.SetTeam (team);

        GraphNodeComponent spawnPoint = null;

        switch (team)
        {
            case Team.Team1:
                spawnPoint = team1SpawnPoints[Random.Range (0, team1SpawnPoints.Count)];
                team1Tanks.Add (newTank);
                break;
            case Team.Team2:
                team2Tanks.Add (newTank);
                spawnPoint = team2SpawnPoints[Random.Range (0, team2SpawnPoints.Count)];
                break;
        }

        newTank.transform.position = spawnPoint.transform.position + Vector3.up + (Quaternion.Euler (0f, Random.Range (-90f, 90f), 0f) * Vector3.right * 2f);
        newTank.transform.forward = spawnPoint.transform.forward;

        //tankComponent.TargetNode = spawnPoint;
    }

    public static void RespawnTank (TankController tank)
    {
        switch (tank.Team)
        {
            case Team.Team1:
                instance.team1Tanks.Remove (tank.gameObject);
                instance.team2RoundScore++;
                break;
            case Team.Team2:
                instance.team2Tanks.Remove (tank.gameObject);
                instance.team1RoundScore++;
                break;
        }

        instance.StartCoroutine (instance.WaitForRespawn (tank.Team));

        Destroy (tank.gameObject);
    }

    IEnumerator WaitForRespawn (Team team)
    {
        yield return new WaitForSeconds (respawnTime);

        SpawnTank (team);
    }

    public static List<Vector3> GetTankPositions ( )
    {
        if (instance.team1Tanks == null || instance.team2Tanks == null) return new List<Vector3> (0);

        List<Vector3> tankPositions = new List<Vector3> ( );

        foreach (var tank in instance.team1Tanks)
        {
            if (tank != null)
                tankPositions.Add (tank.transform.position);
        }

        foreach (var tank in instance.team2Tanks)
        {
            if (tank != null)
                tankPositions.Add (tank.transform.position);
        }

        return tankPositions;
    }

    public static Transform GetRandomTank (Transform previous = null)
    {
        if (previous != null)
        {
            if (previous.GetComponent<TankController> ( ).Team == Team.Team1)
            {
                return GetRandomTank (Team.Team2)?.transform;
            }
            else
            {
                return GetRandomTank (Team.Team1)?.transform;
            }
        }
        else
        {
            return GetRandomTank (Team.Team1)?.transform;
        }
    }

    static GameObject GetRandomTank (Team team)
    {
        switch (team)
        {
            case Team.Team1:
                foreach (var tank in instance.team1Tanks)
                {
                    return tank;
                }
                break;
            case Team.Team2:
                foreach (var tank in instance.team2Tanks)
                {
                    return tank;
                }
                break;
        }

        return null;
    }

    private void OnGUI ( )
    {
        var boxStyle = new GUIStyle ( );
        boxStyle.normal.background = boxBackgroundTexture;

        var richText = new GUIStyle ( );
        richText.richText = true;

        Rect scoreArea = new Rect (0f, 0f, 300f, 300f);

        GUILayout.BeginArea (scoreArea, boxStyle);
        {
            GUILayout.Label ("<b><size=30>Match Info</size></b>");

            GUILayout.Label ($"Current Round: <b>{currentRound}</b>");
            GUILayout.Label ("Current Round Time: " + $"<b>{currentRoundTime.ToString ("#.##")}</b>");

            GUILayout.Label ($"Match Score: <b>{team1MatchScore} - {team2MatchScore}</b>");
            GUILayout.Label ($"Round Score: <b>{team1RoundScore} - {team2RoundScore}</b>");
        }
        GUILayout.EndArea ( );

        if (matchOver)
        {
            Rect matchOverArea = new Rect (Screen.width / 2f - 150f, Screen.height / 2f - 150f, 300f, 300f);

            GUILayout.BeginArea (matchOverArea, boxStyle);
            {
                GUILayout.Label ("<size=50>Match Over</size>", richText);

                string results = $"";

                if (team1MatchScore > team2MatchScore)
                {
                    results = $"<size=40>Team 1 Winner</b>";
                }
                else if (team2MatchScore > team1MatchScore)
                {
                    results = $"<size=40>Team 2 Winner</b>";
                }
                else
                {
                    results = $"<size=40>Draw</b>";
                }

                if (GUILayout.Button ("Restart"))
                {
                    if (Application.isPlaying)
                    {
                        matchOver = false;
                        StartCoroutine (WaitForGameRestart ( ));
                    }
                }
            }
            GUILayout.EndArea ( );

        }
    }

    private Texture2D CreateTexture (int width, int height, Color col)
    {
        Color[ ] pix = new Color[width * height];

        for (int i = 0; i < pix.Length; i++)
            pix[i] = col;

        Texture2D result = new Texture2D (width, height);
        result.SetPixels (pix);
        result.Apply ( );

        return result;
    }
}
